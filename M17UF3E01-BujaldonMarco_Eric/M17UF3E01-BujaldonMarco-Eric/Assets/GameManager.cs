﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static int punts;
    public Text puntuacion;

    // Start is called before the first frame update
    void Start()
    {
        punts = 0;
    }

    // Update is called once per frame
    void Update()
    {
        puntuacion.text = "" + punts;

    }
}
