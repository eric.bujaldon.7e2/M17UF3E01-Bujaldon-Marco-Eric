﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip getpoint;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        getpoint = Resources.Load<AudioClip>("getpoint");

        audioSrc = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "getpoint":
                audioSrc.PlayOneShot(getpoint);
                break;
        }
    }
}

