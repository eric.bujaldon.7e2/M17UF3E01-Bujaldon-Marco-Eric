﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotaScript : MonoBehaviour
{
    public float movementSpeed = 2.0f;
    public Vector3 userDirection = Vector3.forward;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //        transform.position += Vector3.forward * Time.deltaTime * movementSpeed;


        transform.Translate(userDirection * movementSpeed * Time.deltaTime);

    }
}
